﻿#nullable enable

namespace PulsarV3;

public partial class Pulsar
{
	internal byte[] GetIncludeFile( string fileName, out string templateDir )
	{
		var Save = _TemplateName;

		try
		{
			_TemplateName = fileName;

			fileName = IsVariableName( fileName ) ? this[ GetVariableName( fileName ) ] : fileName.Substring( 1, fileName.Length - 2 );

			var Stream = GetTemplate( fileName, out templateDir );

			if( Stream is null )
				throw new Exception( "Missing template: " + fileName );

			return Stream.ToArray();
		}
		finally
		{
			_TemplateName = Save;
		}
	}

	internal string GetIncludeFileAscii( string fileName, out string templateDir )
	{
		var Ascii = new ASCIIEncoding();

		return Ascii.GetString( GetIncludeFile( fileName, out templateDir ) );
	}

	internal string GetIncludeFileUtf8( string fileName, out string templateDir )
	{
		var Utf8 = new UTF8Encoding();

		return Utf8.GetString( GetIncludeFile( fileName, out templateDir ) );
	}

	internal string GetIncludeFileUtf16( string fileName, out string templateDir )
	{
		var Utf16 = new UnicodeEncoding();

		return Utf16.GetString( GetIncludeFile( fileName, out templateDir ) );
	}
}