﻿#nullable enable

namespace PulsarV3;

public partial class Pulsar
{
	public class FunctionArgs : Dictionary<string, string>;

	private readonly Dictionary<string, Func<Pulsar, string, FunctionArgs, string>> BlockFunctions = new();

	private readonly List<string> BlockFunctionStack = new();

	private readonly Dictionary<string, Func<Pulsar, FunctionArgs, string>> Functions = new();

	internal void PushBlockFunctionName( string name )
	{
		BlockFunctionStack.Add( name.Trim() );
	}

	internal string BlockFunctionTopOfStack()
	{
		var Ndx = BlockFunctionStack.Count - 1;

		return Ndx >= 0 ? BlockFunctionStack[ Ndx ] : "";
	}

	internal string PopBlockFunctionName()
	{
		var Ndx = BlockFunctionStack.Count - 1;

		if( Ndx >= 0 )
		{
			var RetVal = BlockFunctionStack[ Ndx ];
			BlockFunctionStack.RemoveAt( Ndx );
			return RetVal;
		}
		return "";
	}

	public Pulsar RegisterFunction( string functionName, Func<Pulsar, FunctionArgs, string>? callback )
	{
		functionName = functionName.Trim();

		if( callback is null )
			Functions.Remove( functionName );
		else
			Functions[ functionName ] = callback;

		return this;
	}

	internal string ExecuteFunction( string functionName, FunctionArgs args )
	{
		functionName = functionName.Trim();

		var P = this;

		do
		{
			if( P.Functions.TryGetValue( functionName, out var Func ) )
				return Func( this, args );

			P = P.Parent;
		}
		while( P is not null );

		return "";
	}

	public Pulsar RegisterBlockFunction( string functionName, Func<Pulsar, string, FunctionArgs, string>? callback )
	{
		functionName = functionName.Trim();

		if( callback is null )
			BlockFunctions.Remove( functionName );
		else
			BlockFunctions[ functionName ] = callback;

		return this;
	}

	internal bool IsBlockFunction( string functionName )
	{
		functionName = functionName.Trim();

		var P = this;

		do
		{
			if( P.BlockFunctions.ContainsKey( functionName ) )
				return true;

			P = P.Parent;
		}
		while( P is not null );

		return false;
	}

	internal string ExecuteBlockFunction( string functionName, string? context, FunctionArgs args )
	{
		functionName = functionName.Trim();

		var P = this;

		do
		{
			if( P.BlockFunctions.TryGetValue( functionName, out var Func ) )
				return Func( this, context!,  args );

			P = P.Parent;
		}
		while( P is not null );

		return "";
	}
}