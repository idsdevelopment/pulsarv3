﻿// ReSharper disable ReplaceSubstringWithRangeIndexer

#nullable enable

namespace PulsarV3;

public partial class Pulsar
{
	public Dictionary<string, Func<object, List<string>, string?>> Modifiers { get; private set; } = new();

	public Pulsar RegisterModifier( string name, Func<object, List<string>, string?> func )
	{
		Modifiers[ name ] = func;
		return this;
	}

	internal Func<object, List<string>, string?>? GetModifier( string name )
	{
		var P = this;

		do
		{
			var M = P.Modifiers;

			if( M.TryGetValue( name, out var Modifier ) )
				return Modifier;

			P = P.Parent;
		}
		while( P is not null );

		return null;
	}

	internal object? ModifyValue( string modifierName, object? value, List<string> args )
	{
		if( value is not null )
		{
			var Func = GetModifier( modifierName );
			return Func is not null ? Func( value, args ) : value;
		}
		return null;
	}

	internal void SetDefaultModifiers()
	{
		static string ObjectToString( object? obj )
		{
			return obj?.ToString() ?? "";
		}

		static string ObjectToLength( object? obj )
		{
			return ObjectToString( obj ).Length.ToString( CultureInfo.InvariantCulture );
		}

		static string ObjectToLower( object? obj )
		{
			return ObjectToString( obj ).ToLower();
		}

		static string ObjectToUpper( object? obj )
		{
			return ObjectToString( obj ).ToUpper();
		}

		RegisterModifier( "Length", ( value, _ ) => ObjectToLength( value ) );

		RegisterModifier( "count_characters", ( value, _ ) => ObjectToLength( value ) );

		RegisterModifier( "capitalise", ( value, _ ) => CultureInfo.CurrentCulture.TextInfo.ToTitleCase( ObjectToLower( value ) ) );

		RegisterModifier( "lower", ( value, _ ) => ObjectToLower( value ) );

		RegisterModifier( "ToLower", ( value, _ ) => ObjectToLower( value ) );

		RegisterModifier( "upper", ( value, _ ) => ObjectToUpper( value ) );

		RegisterModifier( "ToUpper", ( value, _ ) => ObjectToUpper( value ) );

		RegisterModifier( "Trim", ( value, _ ) => ObjectToString( value ).Trim() );

		RegisterModifier( "TrimEnd", ( value, _ ) => ObjectToString( value ).TrimEnd() );

		RegisterModifier( "TrimStart", ( value, _ ) => ObjectToString( value ).TrimStart() );

		RegisterModifier( "Substring", ( value, args ) =>
		                               {
			                               var Str = ObjectToString( value );

			                               var Count = args.Count;

			                               if( Count > 0 )
			                               {
				                               if( !int.TryParse( args[ 0 ], out var Arg0 ) )
					                               Arg0 = 0;

				                               if( Count > 1 )
				                               {
					                               if( !int.TryParse( args[ 0 ], out var Arg1 ) )
						                               Arg1 = 0;

					                               return Str.Substring( Arg0, Arg1 );
				                               }

				                               return Str.Substring( Arg0 );
			                               }

			                               return "";
		                               } );

		RegisterModifier( "Format", ( value, args ) => args.Count > 0
			                                               ? string.Format( args[ 0 ], value )
			                                               : ObjectToString( value )
		                );
	}
}