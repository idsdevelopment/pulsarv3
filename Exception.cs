﻿#nullable enable

namespace PulsarV3;

public class SilentException : Exception;

public class PulsarException : Exception
{
	public PulsarException( string message ) : base( message )
	{
	}
}