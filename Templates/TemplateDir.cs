﻿using System.IO;
using PulsarV3.Utils;

namespace PulsarV3;

public partial class Pulsar
{
	public List<string> TemplateDirectories { get; private set; } = new();

	public string TemplateDirectory => _TemplateDirectory;

	public string CurrentDir => _TemplateDirectory;

	public string TemplateName => _TemplateName;

	public string Template => _TemplateName;

	public Func<string, byte[]> Reader;

	internal MemoryStream GetTemplate( string path, string fileName, out string templatePath )
	{
		var Path = Util.AddPathSeparator( path ) + fileName;

		if( TemplateCache.ContainsKey( Path ) )
		{
			templatePath = Path;

			return new MemoryStream( TemplateCache[ Path ] );
		}

		try
		{
			byte[] Buffer;

			if( Reader is { } )
				Buffer = Reader( Path );
			else
			{
				if( File.Exists( Path ) )
				{
					using var FileStream = new FileStream( Path, FileMode.Open, FileAccess.Read, FileShare.Read );
					var       Len        = (int)FileStream.Length;
					Buffer = new byte[ Len ];

					for( var I = 0; I < Len; )
						I += FileStream.Read( Buffer, I, Len );
				}
				else
					throw new Exception( $"Template does not exist: {Path}" );
			}

			TemplateCache.Add( Path, Buffer );

			templatePath = Path;

			return new MemoryStream( Buffer );
		}
		catch
		{
			throw new PulsarException( "Cannot open file: " + fileName );
		}
	}

	internal MemoryStream GetTemplate( string fileName, out string templatePath )
	{
		fileName = fileName.Trim( '"', '\'' );

		if( fileName.IndexOf( "..", StringComparison.Ordinal ) >= 0 )
			throw new PulsarException( "File name: " + fileName + "cannot contain \"..\"" );

		if( Path.IsPathRooted( fileName ) )
			return GetTemplate( Path.GetDirectoryName( fileName ), Path.GetFileName( fileName ), out templatePath );

		if( TemplateCache.TryGetValue( fileName, out var Buffer ) )
		{
			templatePath = fileName;

			return new MemoryStream( Buffer );
		}

		var Ndx = 0;

		foreach( var Dir in TemplateDirectories )
		{
			var Temp = GetTemplate( Dir, fileName, out templatePath );

			if( Temp is { } )
			{
				if( Ndx != 0 )
				{
					TemplateDirectories.RemoveAt( Ndx );
					TemplateDirectories.Insert( 0, Dir );
				}

				return Temp;
			}

			++Ndx;
		}

		throw new PulsarException( "Template not found: " + fileName );
	}

	public void RegisterTemplate( string fileName, string template )
	{
		TemplateCache.Add( fileName, Encoding.UTF8.GetBytes( template ) );
	}
}