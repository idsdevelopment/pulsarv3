﻿#nullable enable

namespace PulsarV3;

public partial class Pulsar
{
	internal class NewOutputBuffer
	{
		internal class Block
		{
			internal StringBuilder Text
			{
				get
				{
					var Result = new StringBuilder();

					BuildText( this, Result );

					return Result;
				}
			}

			private           bool          IsTextBlock => Blocks.Count == 1;
			private readonly  StringBuilder _Text = new();
			internal readonly string        Name;
			internal readonly Block?        Parent;
			internal          List<Block>   Blocks      = new();
			internal          BLOCK_MODE    CurrentMode = BLOCK_MODE.BASE;

			internal Block LastBlock;

			public override string ToString() => Text.ToString();

			internal void Append( string text )
			{
				if( text != "" )
				{
					if( !LastBlock.IsTextBlock )
						Blocks.Add( LastBlock = new Block( this ) );

					switch( CurrentMode )
					{
					case BLOCK_MODE.BASE:
					case BLOCK_MODE.APPEND:
						LastBlock._Text.Append( text );

						break;

					case BLOCK_MODE.REPLACE:
						_Text.Clear();
						_Text.Append( text );
						CurrentMode = BLOCK_MODE.APPEND;

						break;

					case BLOCK_MODE.PREAPPEND:
						var NewBlock = new Block( this );
						NewBlock._Text.Append( text );
						Blocks.Insert( 0, NewBlock );

						break;
					}
				}
			}

			internal Block AddBlock( Block block )
			{
				Blocks.Add( LastBlock = block );

				return block;
			}

			internal Block AddBlock( string name ) => AddBlock( new Block( this, name ) );

			internal Block AddBlock() => AddBlock( "" );

			internal Block( Block? parent, string name )
			{
				Parent = parent;
				Name   = name;
				Blocks.Add( LastBlock = this );
			}

			internal Block( Block? parent ) : this( parent, "" )
			{
			}

			private static void BuildText( Block block, StringBuilder text )
			{
				text.Append( block._Text );
				var List  = block.Blocks;
				var Count = List.Count;

				for( var I = 1; I < Count; I++ )
					BuildText( List[ I ], text );
			}
		}

		internal string Text => ToString();

		internal Stack<Block> BlockStack = new();

		internal Block CurrentBlock = new( null ),
		               RootBlock    = new( null );

		internal enum BLOCK_MODE
		{
			BASE,
			REPLACE,
			APPEND,
			PREAPPEND
		}

		internal void Clear()
		{
			RootBlock = CurrentBlock = new Block( null );
		}

		internal void Clear( NewOutputBuffer buffer )
		{
			CurrentBlock = RootBlock = buffer.CurrentBlock;
		}

		public override string ToString() => CurrentBlock.Text.ToString();

		internal void Append( string text )
		{
			CurrentBlock.Append( text );
		}

		internal void BeginBlock( string blockName, BLOCK_MODE bMode = BLOCK_MODE.BASE )
		{
			BlockStack.Push( CurrentBlock );

			if( bMode != BLOCK_MODE.BASE )
			{
				var BaseBlock = CurrentBlock;

				while( BaseBlock.Parent is not null )
					BaseBlock = BaseBlock.Parent;

				BeginBlock( BaseBlock.Blocks, blockName, bMode );
			}
			else
				CurrentBlock = CurrentBlock.AddBlock( blockName );
		}

		internal void BeginBlock( Block block )
		{
			BlockStack.Push( CurrentBlock );
			CurrentBlock = CurrentBlock.AddBlock( block );
		}


		internal void EndBlock()
		{
			CurrentBlock = BlockStack.Pop();
			CurrentBlock.AddBlock();
		}

		internal void PushOutputBuffer()
		{
			BlockStack.Push( CurrentBlock );
			Clear();
		}

		internal Block PopOutputBuffer()
		{
			var RetVal = CurrentBlock;
			CurrentBlock = BlockStack.Pop();

			return RetVal;
		}

		internal NewOutputBuffer()
		{
			Clear();
		}

		private bool BeginBlock( IReadOnlyList<Block> blocks, string blockName, BLOCK_MODE bMode = BLOCK_MODE.BASE )
		{
			var Count = blocks.Count;

			for( var I = 1; I < Count; I++ )
			{
				var B1 = blocks[ I ];

				if( B1.Name == blockName )
				{
					B1.CurrentMode = bMode;
					CurrentBlock   = B1;

					return true;
				}

				if( BeginBlock( B1.Blocks, blockName, bMode ) )
					return true;
			}

			return false;
		}
	}

	internal void PushOutputBuffer()
	{
		Output.PushOutputBuffer();
	}

	internal string PopOutputBuffer() => Output.PopOutputBuffer().ToString();
}