﻿#nullable enable

using System.IO;
using System.Linq;
using System.Reflection;
using PulsarV3.Utils;

namespace PulsarV3;

public partial class Pulsar
{
	public const string INITIALISE_MODULE = "INITIALISE_MODULE",
	                    REMOVE_MODULE     = "REMOVE_MODULE";

	public List<string> ModuleDirectories { get; private set; } = new();

	private readonly Dictionary<string, Assembly> LoadedModules = new();

	public PulsarModule LoadModule( string fileName, string @class, Dictionary<string, string>? args )
	{
		if( fileName.IndexOf( "..", StringComparison.Ordinal ) >= 0 )
			throw new PulsarException( "Module " + fileName + "cannot contain \"..\"" );

		var Ext = Path.GetExtension( fileName );

		if( Ext == "" )
			fileName += ".dll";

		foreach( var Pth in ModuleDirectories.Select( Util.AddPathSeparator ).Select( modulePath => modulePath + fileName ) )
		{
			try
			{
				Assembly MyAssembly;
				var      FromCache = LoadedModules.ContainsKey( Pth );

				if( FromCache )
					MyAssembly = LoadedModules[ Pth ];
				else
				{
					if( !File.Exists( Pth ) )
						continue;

					var Name = AssemblyName.GetAssemblyName( Pth );
					MyAssembly = AppDomain.CurrentDomain.Load( Name );

					LoadedModules.Add( Pth, MyAssembly );
				}

				var MyLoadClass = MyAssembly.GetType( @class ) ?? throw new ArgumentNullException( $"{nameof( MyAssembly )}.GetType( @class )" );
				var Obj         = (PulsarModule)Activator.CreateInstance( MyLoadClass )!;
				Obj.InitialiseModule( this, args ?? new Dictionary<string, string>() );

				return Obj;
			}
			catch( SilentException )
			{
				throw;
			}
			catch( Exception E )
			{
				throw new PulsarException( "Cannot load module: " + fileName + " Reason: " + E.Message );
			}
		}

		throw new PulsarException( "Module not found: " + fileName );
	}
}