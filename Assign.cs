﻿#nullable enable

// ReSharper disable InconsistentNaming

namespace PulsarV3;

public partial class Pulsar
{
	private readonly Dictionary<string, object> Variables = new();

	public Pulsar Assign( string variableName, bool value ) => Assign( variableName, value ? "1" : "0" );


	public Pulsar Assign( string variableName, object? value )
	{
		variableName = GetVariableName( variableName );

		if( Variables.ContainsKey( variableName ) )
		{
			if( value is not null )
				Variables[ variableName ] = value;
			else
				Variables.Remove( variableName );
		}
		else if( value is not null )
			Variables.Add( variableName, value );

		return this;
	}

	public object? GetAssignAsObject1Level( string variableName )
	{
		variableName = GetVariableName( variableName );
		return Variables.TryGetValue( variableName, out var Variable ) ? Variable : null;
	}

	public object? GetAssignAsObject( string variableName )
	{
		variableName = GetVariableName( variableName );

		var P = this;

		do
		{
			var V = P.Variables;

			if( V.TryGetValue( variableName, out var O ) )
				return O;

			P = P.Parent;
		}
		while( P is not null );

		return null;
	}


	public string GetAssign( string variableName )
	{
		var Temp = GetAssignAsObject( variableName );

		return Temp?.ToString() ?? "";
	}
}