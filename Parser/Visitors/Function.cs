﻿#nullable enable

namespace PulsarV3.Parser.Visitors;

internal partial class PulsarBaseVisitor
{
	public override ExpressionResultEntry VisitBlockFunction( PulsarParser.BlockFunctionContext context )
	{
		var Func    = context.BLOCK_FUNCTION().GetText().Trim();
		var EndFunc = context.END_BLOCK_FUNCTION().GetText();
		EndFunc = EndFunc.Substring( 1, EndFunc.Length - 2 ).Trim(); // remove '/' and '}'

		if( Func == EndFunc )
		{
			var FArgs = context.functionArgs();
			var Args  = FArgs is not null ? Visit( FArgs ).FunctionArgs : new Pulsar.FunctionArgs();

			PushOutput();

			Output = Pulsar.ExecuteBlockFunction( Func, null, Args ); // Before Tags

			foreach( var Tag in context.tags() )
				Visit( Tag );

			var Body = PopOutput();

			Output = Pulsar.ExecuteBlockFunction( Func, Body, Args );

			return null!;
		}
		throw new PulsarException( "Unexpected {/" + EndFunc + "}" );
	}

	public override ExpressionResultEntry VisitFunction( PulsarParser.FunctionContext context )
	{
		var Func = context.FUNCTION().GetText();

		var FArgs = context.functionArgs();
		var Args  = FArgs is not null ? Visit( FArgs ).FunctionArgs : new Pulsar.FunctionArgs();

		Output = Pulsar.ExecuteFunction( Func, Args );
		return null!;
	}

	public override ExpressionResultEntry VisitFunctionArgs( PulsarParser.FunctionArgsContext context )
	{
		var Result = new ExpressionResultEntry();
		var Args   = new Pulsar.FunctionArgs();

		foreach( var Arg in context.functionArg() )
		{
			var Value = Visit( Arg.functionArgAtom() );

			if( Value is not null )
			{
				var Name = Arg.IDENT().GetText();
				Args.Add( Name, Value.AsString );
			}
		}
		Result.FunctionArgs = Args;
		return Result;
	}

	public override ExpressionResultEntry VisitFunctionArgAtom( PulsarParser.FunctionArgAtomContext context )
	{
		var Number = context.NUMBER();

		if( Number is not null )
		{
			var Result = new ExpressionResultEntry { AsString = Number.GetText() };
			return Result;
		}

		var Type = context.@string();
		return Type is not null ? Visit( Type ) : Visit( context.expressionVariable() );
	}
}