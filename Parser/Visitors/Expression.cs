﻿#nullable enable

// ReSharper disable CompareOfFloatsByEqualityOperator

namespace PulsarV3.Parser.Visitors;

internal partial class PulsarBaseVisitor
{
	public override ExpressionResultEntry VisitExpression( PulsarParser.ExpressionContext context ) => Visit( context.expression1() );

	public override ExpressionResultEntry VisitExpressionBracket( PulsarParser.ExpressionBracketContext context ) => Visit( context.expression1() );

	public override ExpressionResultEntry VisitAssignExpression( PulsarParser.AssignExpressionContext context )
	{
		var ExpVariable = context.assignVariable();

		if( ExpVariable is not null )
		{
			var Variable = ExpVariable.VARIABLE().GetText();
			var Exp      = context.expression();
			var Value    = Visit( Exp );

			if( Value.ObjectVariable is not null )
				Pulsar.SetVariableAsObject( Variable, Value.ObjectVariable, GetVariableScope( ExpVariable.variableScope() ) );
			else
			{
				var Text = Value.AsString;

				// ReSharper disable once LoopCanBeConvertedToQuery
				foreach( var Modifier in ExpVariable.modifiers() )
				{
					var Modifiers = Visit( Modifier ).Modifiers;
					Text = ModifyValue( Modifiers.Name, Text, Modifiers.Args ).ToString();
				}

				if( Text != null )
					Pulsar.SetVariable( Variable, Text, GetVariableScope( ExpVariable.variableScope() ) );
			}
		}

		return null!;
	}

	public override ExpressionResultEntry VisitExpressionNumber( PulsarParser.ExpressionNumberContext context )
	{
		var RetVal = new ExpressionResultEntry { AsString = context.NUMBER().GetText() };

		if( context.MINUS() is not null )
			RetVal.AsNumber = -RetVal.AsNumber;

		return RetVal;
	}

	public override ExpressionResultEntry VisitExpressionMultDiv( PulsarParser.ExpressionMultDivContext context )
	{
		var Expressions = context.expression1();
		var Lval        = Visit( Expressions[ 0 ] );
		var Rval        = Visit( Expressions[ 1 ] );

		if( context.MULT() is not null )
			Lval.AsNumber *= Rval.AsNumber;
		else
		{
			if( context.DIV() is not null )
			{
				var Rv = Rval.AsNumber;
				Lval.AsNumber = Rv != 0 ? Lval.AsNumber / Rv : 0;
			}
			else // MOD
			{
				var Rv = Rval.AsNumber;
				Lval.AsNumber = Rv != 0 ? Lval.AsNumber % Rv : 0;
			}
		}

		return Lval;
	}

	public override ExpressionResultEntry VisitExpressionPlusMinus( PulsarParser.ExpressionPlusMinusContext context )
	{
		var Expressions = context.expression1();
		var Lval        = Visit( Expressions[ 0 ] );
		var Rval        = Visit( Expressions[ 1 ] );

		if( context.PLUS() is not null )
		{
			if( Lval.IsNumber && Rval.IsNumber )
				Lval.AsNumber += Rval.AsNumber;
			else
				Lval.AsString += Rval.AsString;
		}
		else
			Lval.AsNumber -= Rval.AsNumber;

		return Lval;
	}

	public override ExpressionResultEntry VisitExpressionNot( PulsarParser.ExpressionNotContext context )
	{
		var Result = Visit( context.expression1() );

		Result.AsNumber = Result.IsTrue ? 0 : 1;

		return Result;
	}

	public override ExpressionResultEntry VisitExpressionCompare( PulsarParser.ExpressionCompareContext context )
	{
		var Expressions = context.expression1();

		var Lval = Visit( Expressions[ 0 ] );
		var Rval = Visit( Expressions[ 1 ] );

		if( context.EQ() is not null )
		{
			if( Rval.IsNumber && Lval.IsNumber )
				Lval.AsNumber = Lval.AsNumber == Rval.AsNumber ? 1 : 0;
			else
				Lval.AsNumber = Lval.AsString == Rval.AsString ? 1 : 0;
		}
		else if( context.NEQ() is not null )
		{
			if( Rval.IsNumber && Lval.IsNumber )
				Lval.AsNumber = Lval.AsNumber != Rval.AsNumber ? 1 : 0;
			else
				Lval.AsNumber = Lval.AsString != Rval.AsString ? 1 : 0;
		}
		else if( context.GT() is not null )
		{
			if( Rval.IsNumber && Lval.IsNumber )
				Lval.AsNumber = Lval.AsNumber > Rval.AsNumber ? 1 : 0;
			else
				Lval.AsNumber = string.CompareOrdinal( Lval.AsString, Rval.AsString ) > 0 ? 1 : 0;
		}
		else if( context.LT() is not null )
		{
			if( Rval.IsNumber && Lval.IsNumber )
				Lval.AsNumber = Lval.AsNumber < Rval.AsNumber ? 1 : 0;
			else
				Lval.AsNumber = string.CompareOrdinal( Lval.AsString, Rval.AsString ) < 0 ? 1 : 0;
		}
		else if( context.GTE() is not null )
		{
			if( Rval.IsNumber && Lval.IsNumber )
				Lval.AsNumber = Lval.AsNumber >= Rval.AsNumber ? 1 : 0;
			else
				Lval.AsNumber = string.CompareOrdinal( Lval.AsString, Rval.AsString ) >= 0 ? 1 : 0;
		}
		else if( context.LTE() is not null )
		{
			if( Rval.IsNumber && Lval.IsNumber )
				Lval.AsNumber = Lval.AsNumber <= Rval.AsNumber ? 1 : 0;
			else
				Lval.AsNumber = string.CompareOrdinal( Lval.AsString, Rval.AsString ) <= 0 ? 1 : 0;
		}
		else
			Lval.AsNumber = 0;

		return Lval;
	}

	public override ExpressionResultEntry VisitExpressionTernary( PulsarParser.ExpressionTernaryContext context )
	{
		var Result = Visit( context.expression1( 0 ) );

		return Result.AsNumber != 0 ? Visit( context.expression1( 1 ) ) : Visit( context.expression1( 2 ) );
	}
}