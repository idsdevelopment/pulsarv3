﻿#nullable enable

namespace PulsarV3.Parser.Visitors;

internal partial class PulsarBaseVisitor
{
	public override ExpressionResultEntry VisitWhile( PulsarParser.WhileContext context )
	{
		var Expression = context.expression().expression1();
		var Tags       = context.tags();

		while( Visit( Expression ).IsTrue )
		{
			foreach( var Tag in Tags )
				Visit( Tag );
		}

		return null!;
	}

	public override ExpressionResultEntry VisitDoWhile( PulsarParser.DoWhileContext context )
	{
		var Expression = context.expression().expression1();
		var Tags       = context.tags();

		do
		{
			foreach( var Tag in Tags )
				Visit( Tag );
		}
		while( Visit( Expression ).IsTrue );

		return null!;
	}
}