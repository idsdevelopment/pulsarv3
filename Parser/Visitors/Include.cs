﻿#nullable enable

namespace PulsarV3.Parser.Visitors;

internal partial class PulsarBaseVisitor
{
	public override ExpressionResultEntry VisitFetch( PulsarParser.FetchContext context )
	{
		string Text;
		var    TemplateName = context.FILENAME().GetText();

		var Type = context.ASCII();

		if( Type is not null )
			Text = Pulsar.GetIncludeFileAscii( TemplateName, out _ );
		else
		{
			Type = context.UTF16();

			Text = Type is not null
				       ? Pulsar.GetIncludeFileUtf16( TemplateName, out _ )
				       : Pulsar.GetIncludeFileUtf8( TemplateName, out _ );
		}

		Output = Text;

		return null!;
	}

	public override ExpressionResultEntry VisitInclude( PulsarParser.IncludeContext context )
	{
		string Text;
		var    TemplateName = context.FILENAME().GetText();

		var Type = context.UTF8();

		if( Type is not null )
			Text = Pulsar.GetIncludeFileUtf8( TemplateName, out _ );
		else
		{
			Type = context.UTF16();

			Text = Type is not null
				       ? Pulsar.GetIncludeFileUtf16( TemplateName, out _ )
				       : Pulsar.GetIncludeFileAscii( TemplateName, out _ );
		}

		var P = new Pulsar( Pulsar, Pulsar.Output ) { _TemplateName = TemplateName };

		var FArgs = context.functionArgs();

		if( FArgs is not null )
		{
			var Args = Visit( FArgs ).FunctionArgs;

			// No Deconstructor in .Net 4.8
			foreach( var Arg in Args )
				P.Assign( Arg.Key, Arg.Value );
		}

		P.Fetch( Text, buildOutput: false );
		Pulsar.Output.Clear( P.Output );

		return null!;
	}
}