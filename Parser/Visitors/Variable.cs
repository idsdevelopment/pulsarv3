﻿#nullable enable

namespace PulsarV3.Parser.Visitors;

internal partial class PulsarBaseVisitor
{
	public static object? GetProperty( object? obj, string propertyName, out bool isObject )
	{
		isObject = false;

		if( obj is not null )
		{
			if( propertyName.StartsWith( "." ) )
				propertyName = propertyName.Remove( 0, 1 );

			try
			{
				var Property = obj.GetType().GetProperty( propertyName.Trim() );

				// ReSharper disable once UseNullPropagation
				if( Property is not null )
				{
					var Value = Property.GetValue( obj );

					if( Value is not null )
					{
						var PType = Property.PropertyType;

						if( PType.IsPrimitive || ( PType == typeof( string ) ) || ( PType == typeof( decimal ) ) || ( PType == typeof( float ) ) )
						{
							if( PType == typeof( bool ) )
							{
								var Val = Value.ToString();

								return Val == "True" ? "1" : "0";
							}

							return Value.ToString();
						}
						isObject = true;
						return Value;
					}
				}
			}
			catch
			{
			}
		}

		return null;
	}

	public static Pulsar.TEMPLATE_SCOPE GetVariableScope( PulsarParser.VariableScopeContext? context )
	{
		if( context is not null )
		{
			if( context.MODIFIER_SCOPE_GLOBAL() is not null )
				return Pulsar.TEMPLATE_SCOPE.GLOBAL;

			if( context.MODIFIER_SCOPE_PARENT() is not null )
				return Pulsar.TEMPLATE_SCOPE.PARENT;

			if( context.MODIFIER_SCOPE_THIS() is not null )
				return Pulsar.TEMPLATE_SCOPE.THIS;
		}

		return Pulsar.TEMPLATE_SCOPE.TEMPLATE;
	}


	public override ExpressionResultEntry VisitObjectVariable( PulsarParser.ObjectVariableContext context )
	{
		var     Variable = context.VARIABLE().GetText().Trim();
		object? VariableObject;

		var Scope = GetVariableScope( context.variableScope() );

		var Properties    = context.variableProperty();
		var HasProperties = Properties is not null && ( Properties.Length > 0 );

		if( HasProperties )
		{
			var Obj = Pulsar.GetVariableAsObject( Variable, Scope );

			foreach( var Property in Properties! )
			{
				Obj = GetProperty( Obj, Property.GetText(), out var IsObject ) ?? "";

				if( !IsObject )
					break;
			}

			VariableObject = Obj;
		}
		else
			VariableObject = Pulsar.GetVariableAsObject( Variable, Scope );

		return new ExpressionResultEntry
		       {
			       ObjectVariable              = VariableObject,
			       ObjectVariableName          = Variable,
			       ObjectVariableHasProperties = HasProperties
		       };
	}

	public override ExpressionResultEntry VisitExpressionVariable( PulsarParser.ExpressionVariableContext context )
	{
		var     RetVal   = new ExpressionResultEntry();
		var     Variable = context.VARIABLE().GetText();
		object? VariableObject;

		var Scope = GetVariableScope( context.variableScope() );

		var Properties    = context.variableProperty();
		var HasProperties = Properties is not null && ( Properties.Length > 0 );

		if( HasProperties )
		{
			var Obj = Pulsar.GetVariableAsObject( Variable, Scope );

			foreach( var Property in Properties! )
			{
				Obj = GetProperty( Obj, Property.GetText(), out var IsObject ) ?? "";

				if( !IsObject )
					break;
			}

			// ReSharper disable once LoopCanBeConvertedToQuery
			foreach( var Modifier in context.modifiers() )
			{
				var Modifiers = Visit( Modifier ).Modifiers;
				Obj = ModifyValue( Modifiers.Name, Obj, Modifiers.Args );
			}

			VariableObject = Obj;
		}
		else
			VariableObject = Pulsar.GetVariableAsObject( Variable, Scope );

		if( VariableObject is not null )
		{
			var Type     = VariableObject.GetType();
			var IsStruct = Type is { IsValueType: true, IsEnum: false };

			if( !Type.IsPrimitive && ( Type.IsClass || IsStruct ) && VariableObject is not string )
			{
				RetVal.ObjectVariable = VariableObject;
				return RetVal;
			}
		}

		RetVal.AsString = VariableObject is not null ? VariableObject.ToString() ?? "" : "";

		var PreAuto = context.expressionPreAuto();

		if( PreAuto is not null )
		{
			if( HasProperties )
				AutoIncError( Variable );

			if( PreAuto.PRE_INCREMENT_VARIABLE() is not null )
				RetVal.AsNumber += 1;
			else
				RetVal.AsNumber -= 1;

			Pulsar.SetVariable( Variable, RetVal.AsString ?? "", Scope );
		}

		// Post
		var PostAuto = context.expressionPostAuto();

		if( PostAuto is not null )
		{
			if( HasProperties )
				AutoIncError( Variable );

			var Temp = new ExpressionResultEntry { AsString = RetVal.AsString ?? "" };

			if( PostAuto.POST_INCREMENT_VARIABLE() is not null )
				Temp.AsNumber += 1;
			else
				Temp.AsNumber -= 1;

			Pulsar.SetVariable( Variable, Temp.AsString ?? "", Scope );
		}

		var Text = RetVal.AsString;

		if( !HasProperties )
		{
			foreach( var Modifier in context.modifiers() )
			{
				var Modifiers = Visit( Modifier ).Modifiers;
				Text = ModifyValue( Modifiers.Name, VariableObject, Modifiers.Args ).ToString();
			}
		}

		RetVal.AsString = Text ?? "";

		return RetVal;
	}

	public override ExpressionResultEntry VisitVariable( PulsarParser.VariableContext context )
	{
		Output = Visit( context.expressionVariable() ).AsString;

		return null!;
	}

	private static void AutoIncError( string variable )
	{
		throw new ArgumentException( "Pre Increment/Decrement not allowed on variable with property. '" + variable + "'" );
	}
}