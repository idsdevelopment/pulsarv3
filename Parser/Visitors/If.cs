﻿#nullable enable

using System.Linq;

namespace PulsarV3.Parser.Visitors;

internal partial class PulsarBaseVisitor
{
	public override ExpressionResultEntry VisitIf( PulsarParser.IfContext context )
	{
		var IfIsTrue  = Visit( context.expression() ).IsTrue;
		var DidElseIf = false;

		if( IfIsTrue )
		{
			foreach( var Tag in context.tags() )
				Visit( Tag );
		}
		else
		{
			if( context.elseIf().Any( tag => Visit( tag ).IsTrue ) )
				DidElseIf = true;
		}

		if( !IfIsTrue && !DidElseIf )
		{
			var Else = context.@else();

			if( Else is not null )
				Visit( Else );
		}

		return null!;
	}

	public override ExpressionResultEntry VisitElse( PulsarParser.ElseContext context )
	{
		foreach( var Tag in context.tags() )
			Visit( Tag );

		return null!;
	}

	public override ExpressionResultEntry VisitElseIf( PulsarParser.ElseIfContext context )
	{
		var Retval = Visit( context.expression() );

		if( Retval.IsTrue )
		{
			foreach( var Tag in context.tags() )
				Visit( Tag );
		}

		return Retval;
	}
}