﻿#nullable enable

namespace PulsarV3.Parser.Visitors;

internal partial class PulsarBaseVisitor : PulsarParserBaseVisitor<ExpressionResultEntry>
{
	public override ExpressionResultEntry VisitBlockContentOverride( PulsarParser.BlockContentOverrideContext context )
	{
		var BlockName = context.NAME().GetText();
		BlockName = BlockName.Substring( 1, BlockName.Length - 2 ); // Strip Quotes

		Pulsar.NewOutputBuffer.BLOCK_MODE Mode;

		if( context.BLOCK_APPEND() is not null )
			Mode = Pulsar.NewOutputBuffer.BLOCK_MODE.APPEND;
		else if( context.BLOCK_PREAPPEND() is not null )
			Mode = Pulsar.NewOutputBuffer.BLOCK_MODE.PREAPPEND;
		else
			Mode = Pulsar.NewOutputBuffer.BLOCK_MODE.REPLACE;

		BeginBlock( BlockName, Mode );

		foreach( var Tag in context.tags() )
			Visit( Tag );

		EndBlock();

		return null!;
	}

	public override ExpressionResultEntry VisitBaseBlock( PulsarParser.BaseBlockContext context )
	{
		var BlockName = context.NAME().GetText();
		BlockName = BlockName.Substring( 1, BlockName.Length - 2 );

		BeginBlock( BlockName );

		foreach( var Tag in context.tags() )
			Visit( Tag );

		EndBlock();

		return null!;
	}

	private void BeginBlock( string blockName, Pulsar.NewOutputBuffer.BLOCK_MODE mode = Pulsar.NewOutputBuffer.BLOCK_MODE.BASE )
	{
		Pulsar.Output.BeginBlock( blockName, mode );
	}

	private void EndBlock()
	{
		Pulsar.Output.EndBlock();
	}
}