﻿#nullable enable

using Antlr4.Runtime;

namespace PulsarV3.Parser.Visitors;

internal partial class PulsarBaseVisitor
{
	internal string Output
	{
		set => Pulsar.Output.Append( value );

		get => Pulsar.Output.ToString();
	}

	private readonly Pulsar            Pulsar;
	private readonly CommonTokenStream Tokens;

	internal void PushOutput()
	{
		Pulsar.PushOutputBuffer();
	}

	internal string PopOutput() => Pulsar.PopOutputBuffer();

	internal PulsarBaseVisitor( Pulsar smarty, CommonTokenStream tokens )
	{
		Pulsar = smarty;
		Tokens = tokens;
	}
}