﻿// ReSharper disable ReplaceSubstringWithRangeIndexer

#nullable enable

namespace PulsarV3.Parser.Visitors;

internal partial class PulsarBaseVisitor
{
	public override ExpressionResultEntry VisitFor( PulsarParser.ForContext context )
	{
		var OptExpressions = context.optionalExpression();
		var Expression     = context.expression().expression1();
		var Tags           = context.tags();

		Visit( OptExpressions[ 0 ] );

		var DidIteration = false;

		while( Visit( Expression ).IsTrue )
		{
			DidIteration = true;

			foreach( var Tag in Tags )
				Visit( Tag );

			Visit( OptExpressions[ 1 ] );
		}

		if( !DidIteration )
		{
			var ForElse = context.forElse();

			if( ForElse is not null )
			{
				foreach( var Tag in ForElse.tags() )
					Visit( Tag );
			}
		}

		return null!;
	}

	public override ExpressionResultEntry VisitForeachIndex( PulsarParser.ForeachIndexContext context )
	{
		var Variable = context.VARIABLE().GetText().Trim();

		if( Variable.StartsWith( "$" ) )
			Variable = Variable.Substring( 1 );

		return new ExpressionResultEntry { AsString = Variable };
	}

	public override ExpressionResultEntry VisitForEach( PulsarParser.ForEachContext context )
	{
		var DidIteration = false;

		var Index         = context.foreachIndex();
		var IndexVariable = Index is not null ? Visit( Index ).AsString : null;

		var Mode = context.foreachMode();

		var AssignVariable = Mode.assignVariable();

		var DestVariable  = AssignVariable.VARIABLE().GetText().Trim();
		var DestModifiers = AssignVariable.modifiers();

		var SourceVariable = Visit( Mode.objectVariable() );
		var Source         = SourceVariable.ObjectVariable;

		if( Source is IEnumerable Enumerable )
		{
			var LoopIndex = 0;

			foreach( var Obj in Enumerable )
			{
				var TempObj = Obj;

				DidIteration = true;

				if( DestModifiers.Length > 0 ) // Has Modifiers Convert to string
				{
					var Text = TempObj.ToString();

					foreach( var Modifier in DestModifiers )
					{
						var Modifiers = Visit( Modifier ).Modifiers;
						TempObj = ModifyValue( Modifiers.Name, Text, Modifiers.Args );
					}
				}

				Pulsar.Assign( DestVariable, TempObj );

				if( IndexVariable is not null )
					Pulsar.Assign( IndexVariable, LoopIndex++ );

				foreach( var Tag in context.tags() )
					Visit( Tag );
			}
		}

		if( !DidIteration )
		{
			var ForeachElse = context.forEachElse();

			if( ForeachElse is not null )
			{
				foreach( var Tag in ForeachElse.tags() )
					Visit( Tag );
			}
		}

		return null!;
	}
}