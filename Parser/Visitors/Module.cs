﻿namespace PulsarV3.Parser.Visitors;

internal partial class PulsarBaseVisitor
{
	public override ExpressionResultEntry VisitModule( PulsarParser.ModuleContext context )
	{
		var ModuleName = context.FILENAME().GetText().Trim();
		ModuleName = ModuleName.Substring( 1, ModuleName.Length - 2 ).Trim(); // Remove Quotes

		var ClassName = context.CLASS_NAME().GetText().Trim();
		ClassName = ClassName.Substring( 1, ClassName.Length - 2 ).Trim(); // Remove Quotes

		var FArgs = context.functionArgs();
		var Args  = FArgs is not null ? Visit( FArgs ).FunctionArgs : new Dictionary<string, string>();

		Pulsar.LoadModule( ModuleName, ClassName, Args );

		return null;
	}
}