﻿namespace PulsarV3.Parser.Visitors;

internal partial class PulsarBaseVisitor
{
	internal string GetVariableName( string identString ) => Pulsar.GetVariableName( identString );

	internal object ModifyValue( string modifierName, object value, List<string> args )
	{
		// Remove string quotes
		for( var I = 0; I < args.Count; ++I )
			args[ I ] = args[ I ].Trim( '"' );

		return Pulsar.ModifyValue( modifierName, value, args );
	}

	public override ExpressionResultEntry VisitModifiers( PulsarParser.ModifiersContext context )
	{
		var Args = base.VisitModifiers( context ) ?? new ExpressionResultEntry { Modifiers = new ExpressionResultEntry.Modifier { Args = new List<string>() } };

		Args.Modifiers.Name = context.IDENT().GetText();

		return Args;
	}

	public override ExpressionResultEntry VisitModifierArgs( PulsarParser.ModifierArgsContext context )
	{
		var RetVal = new ExpressionResultEntry
		             {
			             Modifiers = new ExpressionResultEntry.Modifier { Args = new List<string>() }
		             };
		var Args = RetVal.Modifiers.Args;

		foreach( var Arg in context.modifierArg() )
		{
			var SArgType = Arg.STRING();

			if( SArgType is not null )
				Args.Add( SArgType.GetText() );
			else
			{
				var NArgType = Arg.NUMBER();

				if( NArgType is not null )
					Args.Add( NArgType.GetText() );
				else
					throw new PulsarException( "Unknown Modifier Arg" );
			}
		}

		return RetVal;
	}
}