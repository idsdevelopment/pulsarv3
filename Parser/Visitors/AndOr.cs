﻿#nullable enable

namespace PulsarV3.Parser.Visitors;

internal partial class PulsarBaseVisitor
{
	public override ExpressionResultEntry VisitExpressionAndOr( PulsarParser.ExpressionAndOrContext context )
	{
		var Expressions = context.expression1();
		var LVal        = Visit( Expressions[ 0 ] );
		var RVal        = Visit( Expressions[ 1 ] );

		if( context.AND() is not null )
			LVal.AsNumber = (int)LVal.AsNumber & (int)RVal.AsNumber;

		else if( context.OR() is not null )
			LVal.AsNumber = (int)LVal.AsNumber | (int)RVal.AsNumber;

		else
			LVal.AsNumber = (int)LVal.AsNumber ^ (int)RVal.AsNumber;

		return LVal;
	}

	public override ExpressionResultEntry VisitExpressionLogicalAndOr( PulsarParser.ExpressionLogicalAndOrContext context )
	{
		var Expressions = context.expression1();

		var Val = Visit( Expressions[ 0 ] );

		if( context.LOG_OR() is not null )
		{
			if( !Val.IsTrue )
				Val = Visit( Expressions[ 1 ] );

			return Val;
		}

		if( Val.IsTrue )
			Val = Visit( Expressions[ 1 ] );

		return Val;
	}
}