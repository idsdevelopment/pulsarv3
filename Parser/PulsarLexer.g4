lexer grammar PulsarLexer;

@lexer::members
{
	private int LiteralDepth = 0;
	private bool ForMode = false;
}

tokens
{
	COMMA,
	IDENT,
	STRING,
	NUMBER,
	EQUALS,
	SEMI_COLON,

	BLOCK_FUNCTION,
	NAME,
}

channels
{
	TEXT_CHANNEL
}

	TAG: '{'													-> pushMode( TAG_MODE ), skip;
	TEXT: ~[{]+;
									
mode TAG_MODE;
	TAG_COMMENT: '*' 											-> skip, mode( COMMENT_MODE );

	TAG_LITERAL: BEGIN_LITERAL{LiteralDepth = 1;} 				-> skip, mode( LITERAL_MODE );

	IGNORE_CRLF: 'ignoreCRLF' CB					 			-> popMode;
	END_IGNORE_CRLF: '/ignoreCRLF' CB					 		-> popMode;

	TAG_VARIABLE: '$'											-> skip, mode( VARIABLE_MODE );

	INCLUDE: 'include' FSPACE+									-> mode( FUNCTION_ARGS_MODE ), pushMode( FILE_MODE );		// Return through args
	FETCH: 'fetch'  FSPACE+										-> mode( FILE_MODE_CB ), pushMode( FILE_MODE );

	EXP: ( 'exp' | 'assign' ) FSPACE+							-> mode( EXPRESSION );
	CLEAR: 'clear' CB											-> popMode;

	IF: 'if' FSPACE+											-> mode( EXPRESSION );
	ELSE: 'else' CB												-> popMode;
	ENDIF: '/if' CB												-> popMode;
	ELSEIF: 'elseif' FSPACE+									-> mode( EXPRESSION );

	WHILE: 'while' FSPACE+										-> mode( EXPRESSION );
	END_WHILE: '/while' CB										-> popMode;
	DO: 'do' CB													-> popMode;
	END_DO: '/do' FSPACE+										-> mode( EXPRESSION );
	END_DO_WHILE: '/while' FSPACE+								-> type( END_DO ), mode( EXPRESSION );
	
	FOR: 'for' FSPACE+ {ForMode = true;}						-> mode( EXPRESSION );
	END_FOR: '/for' CB {ForMode = false;}						-> popMode;
	FORELSE: 'forelse' CB										-> popMode;									

	FOREACH: 'foreach' FSPACE+									-> mode( FOREACH_MODE );
	END_FOREACH: '/foreach' CB									-> popMode;
	FOREACH_ELSE: 'foreachelse' CB								-> popMode;

	CAPTURE: 'capture' FSPACE+									-> mode( CAPTURE_MODE );
	END_CAPTURE: '/capture' CB									-> popMode;

	BLOCK: 'block' FSPACE+										-> mode( BLOCK_MODE );
	END_BLOCK: '/block' CB										-> popMode;
	EXTENDS: 'extends' FSPACE+									-> mode( FUNCTION_ARGS_MODE ), pushMode( FILE_MODE );		// Return through args

	OVERRIDE_BLOCK: 'override' FSPACE+					 		-> mode( BLOCK_MODE );
	END_BLOCK_OVERRIDE: '/override' CB							-> popMode;

	MODULE: 'module' FSPACE+									-> mode( MODULE_MODE );

	SWITCH: 'switch' FSPACE+									-> mode( SWITCH_MODE );
	END_SWITCH: '/switch' CB									-> popMode;

	CASE: 'case' FSPACE+										-> mode( CASE_MODE ); 
	DEFAULT_CASE: 'case' CB										-> popMode;
	END_CASE: '/case' CB FSPACE_NL*								-> popMode;

	FUNCTION: FIDENT FSPACE*											
	{
		var Func = Text.Trim();
		Type = ( Pulsar.IsBlockFunction( Func ) ? BLOCK_FUNCTION : FUNCTION );
	}															-> mode( FUNCTION_ARGS_MODE );		// Must Be last {/if} etc;
	END_BLOCK_FUNCTION: '/' FIDENT CB							-> popMode; 

	BRACE_CHAR: '{'												-> popMode;

	BRACE_TEXT:	.												-> popMode;


mode SWITCH_MODE;
	SWITCH_VARIABLE: FVARIABLE									-> type( VARIABLE ), pushMode( MODIFIER_MODE );
	SWITCH_CB: CB FSPACE_NL*									-> popMode;

mode CASE_MODE;
	CASE_STRING: FSTRING										-> type( STRING );
	CASE_MUMBER: FNUMBER										-> type( NUMBER );
	CASE_CB: CB													-> popMode;


mode MODULE_MODE;
	MODULE_FILENAME: ( '\'' FPATH_CHARACTERS+ '\''
					 | '"' FPATH_CHARACTERS+ '"' )				-> type( FILENAME ), mode( MODULE1_MODE );

mode MODULE1_MODE;
	CLASS_SEPARATOR: ( FSPACE | FSPACE* ',' ) FSPACE*			-> type( COMMA );
	CLASS_NAME:		FMODULE_IDENT FSPACE*						-> mode( FUNCTION_ARGS_MODE );


mode BLOCK_MODE;
	BLOCK_NAME_TAG: FNAME_TAG									-> skip;
	BLOCK_NAME: FQUOTED_IDENT									-> type( NAME );
	BLOCK_HIDE: FSPACE* FHIDE;
	BLOCK_APPEND: FSPACE* FAPPEND;
	BLOCK_PREAPPEND: FSPACE* FPREAPPEND;
	BLOCK_CB: CB												-> popMode;
													 
mode CAPTURE_MODE;
	CAPTURE_VARIABLE: FVARIABLE									-> type( VARIABLE );
	CAPTURE_CB: CB												-> popMode;

mode FOREACH_MODE;
	FOREACH_INDEX: FSPACE* FINDEX_TAG;
	FOREACH_VARIABLE: FSPACE* FVARIABLE							-> type( VARIABLE );
	IN: FSPACE+ FIN FSPACE+;
	AS: FSPACE+ FAS FSPACE+; 
	FOREACH_CB: CB												-> popMode;
	
	FOREACH_MODIFIER_SCOPE_PARENT: FSCOPE_PARENT				-> type( MODIFIER_SCOPE_PARENT );
	FOREACH_MODIFIER_SCOPE_GLOBAL: FSCOPE_GLOBAL				-> type( MODIFIER_SCOPE_GLOBAL );
	FOREACH_MODIFIER_SCOPE_THIS: FSCOPE_THIS					-> type( MODIFIER_SCOPE_THIS );
	FOREACH_MODIFIER_PROPERTY: '.' FIDENT						-> type( MODIFIER_PROPERTY );


mode FUNCTION_ARGS_MODE;
	FUNC_IDENT: FIDENT											-> type( IDENT );
	FUNC_VARIABLE: FVARIABLE									-> type( VARIABLE ), pushMode( MODIFIER_MODE );
	FUNC_STRING: FSTRING										-> type( STRING ), pushMode( MODIFIER_MODE );
	FUNC_MUMBER: FNUMBER										-> type( NUMBER );
	FUNC_EQUALS: '='											-> type( EQUALS );		
	FUNC_ARG_SEPARATOR: ','	FSPACE*								-> type( COMMA );
	END_FUNCTION_MODE: CB										-> popMode;
	FUNC_WS: FSPACE												-> skip;

mode EXPRESSION;
	EXP_VARIABLE: FVARIABLE										-> type( VARIABLE ), pushMode( MODIFIER_MODE );
	EXP_STRING: FSTRING											-> type( STRING ), pushMode( MODIFIER_MODE );
	EXP_MUMBER: FNUMBER											-> type( NUMBER );

	OPEN_BRACKET: '(';
	CLOSE_BRACKET: ')';

	TERNARY: FSPACE* '?' FSPACE*;
	TERNARY_ALTERNATIVE: FSPACE* ':' FSPACE*;
	
	EQ:	 ( FSPACE* '==' FSPACE* | FSPACE+ 'eq' FSPACE+ );
	NEQ: ( FSPACE* '!=' FSPACE* | FSPACE+ 'neq' FSPACE+ | FSPACE+ 'ne' FSPACE+ );
	GT: ( FSPACE* '>' FSPACE* | FSPACE+ 'gt' FSPACE+ );
	LT: ( FSPACE* '<' FSPACE* | FSPACE+ 'lt' FSPACE+ );
	GTE: ( FSPACE* '>=' FSPACE* | FSPACE+ 'gte' FSPACE+ );
	LTE: ( FSPACE* '<=' FSPACE* | FSPACE+ 'lte' FSPACE+ );
	NOT: ( FSPACE* '!' FSPACE* | FSPACE+ 'not' FSPACE+ );
	MOD: ( FSPACE* '%' FSPACE* | FSPACE+ 'mod' FSPACE+ );

	LOG_AND: FSPACE* '&&' FSPACE*;
	LOG_OR: FSPACE* '||' FSPACE*;

	EQUALS: FSPACE* '=' FSPACE*;

	PRE_INCREMENT_VARIABLE: '++';
	PRE_DECREMENT_VARIABLE: '--';
	
	PLUS: FSPACE* '+' FSPACE*;
	MINUS: FSPACE* '-' FSPACE*;
	MULT: FSPACE* '*' FSPACE*;
	DIV: FSPACE* '/' FSPACE*;
	AND: FSPACE* '&' FSPACE*;
	OR: FSPACE* '|' FSPACE*;
	XOR: FSPACE* '^' FSPACE*;

	EXP_COMMA: FCOMMA										-> type( COMMA );
	END_EXPRESSION_FOR: FSEMI_COLON{ForMode}?				-> type( END_EXPRESSION ); 
	END_EXPRESSION: CB {ForMode = false;}					-> popMode; 

mode FILE_MODE_CB;
	 FILE_MODE_CB_CB: CB									-> skip, popMode
	 ;

mode FILE_MODE;
	 ASCII: FASCII											-> mode( PATH_MODE );
	 UTF8: FUTF8											-> mode( PATH_MODE );
	 UTF16: FUTF16											-> mode( PATH_MODE );
	 EMPTY_FILE_TYPE:										-> skip, mode( PATH_MODE );

mode PATH_MODE;
	FILENAME: ( '\'' FPATH_CHARACTERS+ '\''
			   | '"' FPATH_CHARACTERS+ '"'
			   | FVARIABLE ) 									->popMode;
	
mode COMMENT_MODE;
	 COMMENT:  .*? '*}'											-> skip, popMode;

mode LITERAL_MODE;
	NESTED_LITERAL: '{' BEGIN_LITERAL{ LiteralDepth++; } 		-> pushMode( LITERAL_MODE ); 
	LITERAL: '{/literal' CB
	{ 
		Type = (--LiteralDepth > 0 ? NESTED_LITERAL : LITERAL); 
	}															-> popMode;
	LITERAL_BRACE: '{';
	LITERAL_TEXT: ~[{]+;

mode VARIABLE_MODE;
	VARIABLE: FIDENT											-> pushMode( MODIFIER_MODE );
	END_VARIABLE_MODE: CB										-> skip, popMode;

mode MODIFIER_MODE;
	POST_INCREMENT_VARIABLE: '++';
	POST_DECREMENT_VARIABLE: '--';

	MODIFIER_SCOPE_PARENT: FSCOPE_PARENT;
	MODIFIER_SCOPE_GLOBAL: FSCOPE_GLOBAL;
	MODIFIER_SCOPE_THIS: FSCOPE_THIS;

	MODIFIER_PROPERTY: '.' FIDENT;
	MODIFIER: '|';	
	MODIFIER_ID: FIDENT											-> type( IDENT );
	MODIFIER_ARGS: ':';
	MODIFIER_ARG_SEPARATOR: ','									-> type( COMMA );
	MODIFIER_STRING: FSTRING									-> type( STRING );
	MODIFIER_NUMBER: FNUMBER									-> type( NUMBER );
	END_MODIFIER_MODE:											-> skip, popMode;				// Empty

fragment FNAME_TAG: 'name' FSPACE* '=' FSPACE*;

fragment FINDEX_TAG: 'index' FSPACE* '=' FSPACE*;
fragment FIN: 'in';
fragment FAS: 'as';

fragment FAPPEND: 'append';
fragment FPREAPPEND: 'preappend';
fragment FHIDE: 'hide';
fragment FREPLACE: 'replace';

fragment FQUOTED_IDENT: '\'' FIDENT '\'' | '"' FIDENT '"';

fragment FMODULE_IDENT: '\'' FDOT_IDENT '\'' | '"' FDOT_IDENT '"';
fragment FDOT_IDENT: FIDENT ( '.' FIDENT )*;

fragment FASCII: [Aa][s][c][i][i] FSPACE+;
fragment FUTF8:  [Uu][t][f][8] FSPACE+;
fragment FUTF16: ( [Uu][t][f][1][6] | [Uu][n][i][c][o][d][e] ) FSPACE+;
fragment FPATH_CHARACTERS: ~[\u0000-\u001f"*:<>?/|\u007f-\uffff];

fragment FCOMMA: ',';
fragment FSPACE: ( ' ' | '\t' );
fragment FSPACE_NL: ( FSPACE | '\n' | '\r' );

fragment CB: FSPACE* RBRACE;
fragment RBRACE: '}';
fragment FSEMI_COLON: FSPACE* ';' FSPACE*;

fragment FIDENT: A_Z_ ( A_Z_ | DIGIT )*;
fragment A_Z_: [A-Za-z_];
fragment DIGIT: [0-9];

fragment FVARIABLE: '$' FIDENT;
fragment FSTRING: ( '"' .*? '"' | '\'' .*? '\'' );
fragment FNUMBER: (DIGIT | '_' )+ ( '.' ( DIGIT | '_' )+ )?;

fragment BEGIN_LITERAL: 'literal' CB;

fragment FSCOPE: '::';
fragment FSCOPE_GLOBAL: FSCOPE [Gg][Ll][Oo][Bb][Aa][Ll];
fragment FSCOPE_PARENT: FSCOPE [Pp][Aa][Rr][Ee][Nn][Tt];
fragment FSCOPE_THIS: FSCOPE [Tt][Hh][Ii][Ss];
	