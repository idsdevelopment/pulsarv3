parser grammar PulsarParser;

options
	{ 
		tokenVocab=PulsarLexer; 		// Use tokens from lexer
	}	

parse: ( capture | TEXT )* extends EOF
	| tags* EOF
	| EOF
	;

tags: tagsNoBlock
	| baseBlock
	| explicitOverrideBlock
	;

tagsNoBlock: text
	| braceChar
	| literal
	| ignoreCRLF
	| variable
	| include
	| fetch
	| exp
	| if
	| function
	| blockFunction
	| while
	| doWhile
	| for
	| forEach
	| capture
	| module
	| clear
	| switch
	;

switch: SWITCH expressionVariable SWITCH_CB case* END_SWITCH
	;

case: CASE ( STRING | NUMBER )? CASE_CB tags* END_CASE
	  | DEFAULT_CASE
	;

clear: CLEAR
	;

include: ( INCLUDE | FETCH ) ( ASCII | UTF8 | UTF16 )? FILENAME functionArgs? END_FUNCTION_MODE
	;

fetch: FETCH ( ASCII | UTF8 | UTF16 )? FILENAME
	;

module: MODULE FILENAME COMMA CLASS_NAME functionArgs? END_FUNCTION_MODE
	;

extends: EXTENDS ( ASCII | UTF8 | UTF16 )? FILENAME functionArgs? END_FUNCTION_MODE ( overrideBlock | tagsNoBlock )*
	;

overrideBlock: BLOCK blockContentOverride endBlock
	;

explicitOverrideBlock: OVERRIDE_BLOCK blockContentOverride END_BLOCK_OVERRIDE
	;

blockContentOverride: NAME ( BLOCK_APPEND | BLOCK_PREAPPEND )? BLOCK_CB tags*
	;

baseBlock: BLOCK NAME BLOCK_HIDE? BLOCK_CB tags* endBlock
	;

endBlock: END_BLOCK
	;

capture: CAPTURE VARIABLE CAPTURE_CB tags* END_CAPTURE
	;

forEach: FOREACH foreachIndex? foreachMode FOREACH_CB tags* forEachElse? END_FOREACH
	;

foreachIndex: FOREACH_INDEX VARIABLE
	;

foreachMode: assignVariable IN objectVariable
	| objectVariable AS assignVariable
	;

objectVariable: VARIABLE variableScope? variableProperty*
	;

forEachElse: FOREACH_ELSE tags*
	;

for: FOR optionalExpression expression optionalExpression tags* forElse? END_FOR
	;

forElse: FORELSE tags*
	;

doWhile: DO tags* END_DO expression
	;

while: WHILE expression tags* END_WHILE
	;

blockFunction: BLOCK_FUNCTION functionArgs? END_FUNCTION_MODE tags* END_BLOCK_FUNCTION
	;

function: FUNCTION functionArgs? END_FUNCTION_MODE
	;

functionArgs: functionArg ( COMMA functionArg )*
	;

functionArg: IDENT EQUALS functionArgAtom
	;

functionArgAtom: string
				| NUMBER
				| expressionVariable
	;

if: IF expression tags* elseIf* else? ENDIF
	;

elseIf: ELSEIF expression tags*
	;

else: ELSE tags*
	;
	 
exp: EXP assignExpression
	;

optionalExpression: ( assignExpression | expression1 END_EXPRESSION )?
	;

assignExpression: <assoc=right> assignVariable EQUALS expression ( COMMA assignExpression )*
	;

expression: expression1 END_EXPRESSION
	;

expression1: OPEN_BRACKET expression1 CLOSE_BRACKET											#ExpressionBracket
		  | expression1 TERNARY expression1 TERNARY_ALTERNATIVE expression1					#ExpressionTernary
		  | NOT expression1																	#ExpressionNot
		  | expression1 ( MULT | DIV | MOD ) expression1									#ExpressionMultDiv
		  | expression1 ( PLUS | MINUS ) expression1										#ExpressionPlusMinus
		  | expression1 ( AND | OR | XOR ) expression1										#ExpressionAndOr
		  | expression1 ( EQ | NEQ | GT | LT | GTE | LTE ) expression1						#ExpressionCompare
		  | expression1 ( LOG_AND | LOG_OR ) expression1									#ExpressionLogicalAndOr
		  | string																			#ExpressionString
		  | expressionVariable																#ExpressionVar
		  | MINUS? NUMBER																	#ExpressionNumber
	;	

assignVariable: VARIABLE variableScope? modifiers*
	;
	
expressionVariable: ( expressionPreAuto? VARIABLE variableScope? variableProperty* expressionPostAuto? ) modifiers*
	;


variableScope: MODIFIER_SCOPE_PARENT | MODIFIER_SCOPE_GLOBAL | MODIFIER_SCOPE_THIS
	;

variableProperty: MODIFIER_PROPERTY
	;

expressionPreAuto: PRE_INCREMENT_VARIABLE | PRE_DECREMENT_VARIABLE
	;
	
expressionPostAuto: POST_INCREMENT_VARIABLE | POST_DECREMENT_VARIABLE
	;

variable: expressionVariable
	;

modifiers: MODIFIER IDENT modifierArgs*
	;

modifierArgs: MODIFIER_ARGS modifierArg ( COMMA modifierArg )*
	;

modifierArg: STRING
	| NUMBER
	;

string: STRING modifiers*
	;

text: TEXT
	| BRACE_TEXT
	;

braceChar: BRACE_CHAR
	;

literal: literalText* LITERAL
		; 

literalText: LITERAL_TEXT
	| LITERAL_BRACE
	| NESTED_LITERAL
	;

ignoreCRLF: IGNORE_CRLF tags* END_IGNORE_CRLF
	; 
