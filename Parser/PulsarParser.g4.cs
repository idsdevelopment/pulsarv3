﻿using PulsarV3.Lexer;

namespace PulsarV3.Parser;

public partial class PulsarParser
{
	internal bool        HasErrors = false;
	internal PulsarLexer Lexer     = null;
	internal Pulsar      Pulsar    = null;
}